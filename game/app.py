from os import path
from typing import List

import pygame

from game.field import Field
from game.camera import Camera
from game.enemy import Enemy
from game.food import Food
from game.player import Player
from game.stone import Stone


class Game:
    def __init__(self, game_params: dict):
        # CONSTANTS
        self.FONT_SIZE = 28
        self.LINE_SPACING = self.FONT_SIZE * 0.25
        self.FPS = 15
        self.CELL_SIZE = 32
        self.ENEMIES_TIMEOUT = 5
        # Private attributes
        self._is_running = False
        self._clock = None
        self._stones: List[Stone] = []
        self._foods: List[Food] = []
        self._enemies: List[Enemy] = []
        self._ticks = 0
        self._score = 0
        # Public attributes
        self.size = self.width, self.height = game_params.get('screen_size', (640, 480))
        self.field_size = game_params.get('field_size', (20, 20))
        self.enemies_count = game_params.get('enemies_count', 10)
        self.stone_count = game_params.get('stone_count', 10)
        self.food_count = game_params.get('food_count', 10)
        self.display = None
        self.field = None
        self.player = None
        self.camera = None
        # Init
        self._initialize_pygame()

    def run(self):
        self._start_screen()

    def _initialize_pygame(self):
        pygame.init()
        pygame.key.set_repeat(100, 0)
        pygame.display.set_caption('IE Game')
        self.display = pygame.display.set_mode(self.size)
        self._clock = pygame.time.Clock()
        self._font = pygame.font.Font(path.join('game', 'resources', 'pixel-font.ttf'), self.FONT_SIZE)

    def _show_text_screen(self, text, event_handler):
        line_pos = (self.height - len(text) * self.FONT_SIZE - (len(text) - 1) * self.LINE_SPACING) // 2
        self.display.fill((0, 0, 0))
        for line in text:
            line_rendered = self._font.render(line, 1, pygame.Color('white'))
            rect = line_rendered.get_rect()
            rect.top = line_pos
            rect.x = 10
            self.display.blit(line_rendered, rect)
            line_pos += self.LINE_SPACING + self.FONT_SIZE

        while True:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    self.stop()
                    return
                elif not event_handler(event):
                    return
            pygame.display.flip()
            self._clock.tick(self.FPS)

    def _start_screen(self):
        text = ['IE Life', '',
                'Помогите IE собрать как можно больше багов.',
                'Будьте осторожны!',
                'Chrome и Firefox могу его легко заменить...', '',
                'Press any key to start...']

        def event_handler(event):
            if event.type in (pygame.KEYDOWN, pygame.MOUSEBUTTONDOWN):
                self._start_game()
                return False
            return True

        self._show_text_screen(text, event_handler)

    def _start_game(self):
        self._score = 0
        self.field = Field(self)
        self.player = Player(self, (self.field_size[0] // 2, self.field_size[0] // 2))
        self.camera = Camera(self)
        self._stones = []
        self._foods = []
        self._enemies = []
        for _ in range(self.food_count):
            food = Food(self, self.field.get_random_empty_cell())
            food.put_on_field()
        for _ in range(self.stone_count):
            stone = Stone(self, self.field.get_random_empty_cell())
            stone.put_on_field()
        for _ in range(self.enemies_count):
            enemy = Enemy(self, self.field.get_random_empty_cell())
            enemy.put_on_field()
            self._enemies.append(enemy)
        self._execute()

    def _finish_game(self):
        text = ['Вот и всё...', '',
                f'Вы набрали багов: {self._score}', '',
                'Press SPACE to restart...']

        def event_handler(event):
            if event.type == pygame.KEYDOWN and event.key == pygame.K_SPACE:
                self._start_game()
                return False
            return True

        self._show_text_screen(text, event_handler)

    def _move_enemies(self):
        if self._ticks % self.ENEMIES_TIMEOUT != 0:
            return
        for enemy in self._enemies:
            enemy.move_random()

    def _handle_event(self, event):
        if event.type == pygame.QUIT:
            self.stop()
            return False
        elif event.type == pygame.KEYDOWN:
            moving = {
                pygame.K_LEFT: (-1, 0),
                pygame.K_RIGHT: (1, 0),
                pygame.K_UP: (0, -1),
                pygame.K_DOWN: (0, 1)
            }
            if event.key in moving:
                self.player.move(moving[event.key][0] + self.player.position[0],
                                 moving[event.key][1] + self.player.position[1])
        return True

    def _check_enemy_attack(self):
        if type(self.field.get(*self.player.position)) == Enemy:
            self._is_running = False

    def _check_eating(self):
        if type(self.field.get(*self.player.position)) == Food:
            self._score += 1
            self.field.remove(*self.player.position)

    def _loop(self):
        self.camera.update(self.player.position)
        self._move_enemies()
        self._check_enemy_attack()
        self._check_eating()

    def _render(self):
        self.display.fill((0, 0, 0))
        self.field.render()
        self.player.render()

    def _execute(self):
        pygame.display.flip()
        self._is_running = True
        while self._is_running:
            for event in pygame.event.get():
                if not self._handle_event(event):
                    return
            self._loop()
            self._render()
            pygame.display.flip()
            self._clock.tick(self.FPS)
            self._ticks += 1
        self._finish_game()

    def stop(self):
        self._is_running = False
        pygame.quit()
