class Camera:
    def __init__(self, game):
        self._screen_size = game.width // game.CELL_SIZE, game.height // game.CELL_SIZE
        self._delta = [0, 0]
        self._border = 2

    def update(self, player_position):
        p = self.apply(*player_position)

        for i in range(2):
            if p[i] < 0 + self._border:
                self._delta[i] = player_position[i] - self._border
            if p[i] >= self._screen_size[i] - self._border:
                self._delta[i] = player_position[i] - (self._screen_size[i] - 1) + self._border

    def apply(self, x, y):
        return x - self._delta[0], y - self._delta[1]
