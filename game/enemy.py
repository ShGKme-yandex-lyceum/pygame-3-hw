import random

from game.game_image_object import GameImageObject


class Enemy(GameImageObject):
    def __init__(self, game, position):
        super().__init__(game, position, random.choice(['firefox.png'] + 4 * ['chrome.png']))

    def can_move(self, x, y):
        return super().can_move(x, y) and type(self._game.field.get(x, y)) != Enemy

    def move_random(self):
        while True:
            pos = [0, 0]
            pos[random.choice([0, 1])] += random.choice([-1, 1])
            if self.move_to(*pos):
                break
