import random

import pygame

from game.stone import Stone


class Field:
    def __init__(self, game):
        self._game = game
        self.width, self.height = self._game.field_size
        self._objects = [[None] * self.width for _ in range(self.height)]
        self.cell_size = self._game.CELL_SIZE

    def _render_cell(self, i, j):
        x, y = self.get_cell_position(*self._game.camera.apply(i, j))
        pygame.draw.rect(self._game.display,
                         pygame.Color('white'),
                         (x, y, self.cell_size, self.cell_size),
                         0)
        pygame.draw.rect(self._game.display,
                         pygame.Color('#999999'),
                         (x - 1, y - 1, self.cell_size + 1, self.cell_size + 1),
                         1)

    def _render_field(self):
        for i in range(self.width):
            for j in range(self.height):
                self._render_cell(i, j)

    def _render_objects(self):
        for i in range(self.width):
            for j in range(self.height):
                if self._objects[i][j]:
                    self._objects[i][j].render()

    def render(self):
        self._render_field()
        self._render_objects()

    def get(self, x, y):
        return self._objects[x][y]

    def put(self, x, y, value):
        self._objects[x][y] = value

    def remove(self, x, y):
        self._objects[x][y] = None

    def replace(self, from_pos, to_pos):
        self.put(*to_pos, self.get(*from_pos))
        self.remove(*from_pos)

    def is_cell_on_field(self, i, j):
        if i < 0 or j < 0 or \
           i >= self.width or j >= self.height:
            return False
        return True

    def is_free_cell(self, i, j):
        if type(self._objects[i][j]) == Stone:
            return False
        return True

    def is_empty_cell(self, i, j):
        return not self._objects[i][j]

    def get_cell_position(self, i, j):
        return i * self.cell_size, j * self.cell_size

    def on_click(self, cell):
        pass

    def get_cell(self, mouse_pos):
        cell_x = (mouse_pos[0]) // self.cell_size
        cell_y = (mouse_pos[1]) // self.cell_size
        if cell_x < 0 or cell_x >= self.width or cell_y < 0 or cell_y >= self.height:
            return None
        return cell_x, cell_y

    def get_click(self, mouse_pos):
        cell = self.get_cell(mouse_pos)
        if cell and cell < (self.width, self.height):
            self.on_click(cell)

    def add_object(self, object):
        self.put(*object.position, object)

    def get_random_empty_cell(self):
        while True:
            i = random.randrange(0, self.width)
            j = random.randrange(0, self.width)
            if self.is_empty_cell(i, j):
                return i, j
