# PyGame: игры на клетчатом поле, спрайты и стокновения объектов

## Домашнее задание

Разработать свою игру с движением, реакцией на действия пользователей и камерой.

## Требования

Требуется Python 3.7+ и установка всех зависимостей:
```shell script
pip install -r requirements.txt
```

## Запуск

```shell script
python -m game
```

## Try it online!

Игра доступна онлайн по адресу: 
[https://repl.it/@ShGKme/pygame-homework](https://repl.it/@ShGKme/pygame-homework)
