class GameObject:
    def __init__(self, game, position):
        self._game = game
        self.position = list(position)
        self._is_on_field = True

    def put_on_field(self):
        self._game.field.add_object(self)

    def set_position(self, x, y):
        self.position = [x, y]

    def can_move(self, x, y):
        if not self._game.field.is_cell_on_field(x, y):
            return False
        if not self._game.field.is_free_cell(x, y):
            return False
        return True

    def move(self, x, y):
        if not self.can_move(x, y):
            return False
        if self._is_on_field:
            self._game.field.replace(self.position, (x, y))
        self.set_position(x, y)
        return True

    def move_to(self, dx, dy):
        return self.move(self.position[0] + dx, self.position[1] + dy)

    def render(self):
        pass
