from game.game_image_object import GameImageObject


class Player(GameImageObject):
    def __init__(self, game, position):
        super().__init__(game, position, 'IE.png')
        self._is_on_field = False
