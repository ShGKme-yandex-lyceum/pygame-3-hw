from game.app import Game

if __name__ == '__main__':
    game = Game({
        'screen_size': (640, 480),
        'field_size': (20, 20),
        'enemies_count': 10,
        'stone_count': 30,
        'food_count': 30,
    })
    game.run()
