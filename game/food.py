from game.game_image_object import GameImageObject


class Food(GameImageObject):
    def __init__(self, game, position):
        super().__init__(game, position, 'bug.png')
