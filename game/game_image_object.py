import pygame

from game.game_object import GameObject
from game.utils import load_image


class GameImageObject(GameObject):
    def __init__(self, game, position, image_filename):
        super().__init__(game, position)
        self.image = pygame.transform.scale(load_image(image_filename), (self._game.field.cell_size, self._game.field.cell_size))

    def render(self):
        super().render()
        coords = list(self._game.field.get_cell_position(*self._game.camera.apply(*self.position)))
        self._game.display.blit(self.image, coords)
