import os

import pygame


def load_image(name):
    fullname = os.path.join('game', 'resources', name)
    try:
        image = pygame.image.load(fullname)
    except pygame.error as message:
        raise SystemExit(message)
    return image
